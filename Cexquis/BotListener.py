import random
import mastodon
import Consts
import  pickle
from TootHTMLParser import TootHTMLParser
import threading
import Log
import os
from collections import deque
import re

#TODO : lock before and after modifying waitIDs & guessIDs


#a Mastodon Stream Listener  defines functions to react when something happens on Mastodon. We inherit it.
class BotListener(mastodon.StreamListener):
    def __init__(self, bot):
        self.bot = bot
        self.waitIDs = {}

    #What happens if the bot receives a notification ? We only want to react to DM.
    def on_notification(self, notification):
        #We react to mentions only
        if(notification['type'] != 'mention'):
            Log.logprint("nevermind, it's not a mention")
            return

        #So there is a toot !
        status = notification['status']

        if(status["visibility"] != "direct"):
            Log.logprint("nevermind, it's not a DM")
            return


        #And there is a text in this toot. But it is mixed with HTML we don't want so we get rid of it.
        content = str(status["content"])
        parser = TootHTMLParser()
        parser.feed(content)
        content = (parser.txt)

        Log.logprint(content)


        atUser = status["account"]

        #If the toot is not in answer to a drawing
        answerTo = status["in_reply_to_id"]
        isAnswer, svcKey, gameName = self.inList(answerTo)

        svc = ["sujet", "verbe", "complément"]
        svcKey = svc[svcKey]

        if(not isAnswer):
            if(re.search(r'[jJ]ouer', content)!=None):
                mentions = status["mentions"]
                if(len(mentions) != 3):
                    self.bot.toot("Pour jouer il faut donner le nom de exactement deux autres personnes avec qui jouer.", status, True)
                else:
                    users = [atUser]
                    for m in mentions:
                        if(m["username"] != Consts.botName):
                            users.append(m)

                    if(len(users) != 3):
                        self.bot.toot("Pour jouer il faut donner le nom de exactement deux autres personnes avec qui jouer.", status, True)
                    else:
                        gameName = ""
                        for user in users:
                            gameName += user["acct"]
                        gameName += ".dat"
                        filename = Consts.gameDirectory + gameName
                        if(not os.path.exists(Consts.gameDirectory)):
                            os.makedirs(Consts.gameDirectory)
                        if(not os.path.exists(filename)): #i.e. pas de parties en cours entre ces trois zigottos
                            data = {"sujet": "", "verbe" : "", "complément" : ""}
                            with open(filename, 'wb') as output:  # Overwrites any existing file.
                                pickle.dump(data, output, pickle.HIGHEST_PROTOCOL)

                            random.shuffle(users)

                            ids = []
                            for i,user in enumerate(users):
                                t = self.bot.toot("@" + user["acct"]+" Bonjour, on m'a demandé de jouer avec toi.\n\nPourrais-tu me donner un " + svc[i] + " s'il te plait ?", None, True)
                                ids.append(t["id"])
                            self.waitIDs[gameName] = [ids, svc, users]

                        else:
                            self.bot.toot("Il y a déjà une partie en cours entre vous 3.", status, True)

        else:  #Si en réponse à une demande de svc
            content = content.replace("@"+Consts.botName, "").replace("\n", "").strip()
            users = self.waitIDs[gameName][2]
            filename = Consts.gameDirectory + gameName
            data = None
            try:
                with open(filename, 'rb') as input:
                    data =  pickle.load(input)
            except FileNotFoundError:
                Log.logprint("[ERROR] oulala je trouve pas le fichier correspondant à la partie", Consts.verboseError)
                for id in self.waitIDs[gameName][0]:
                    self.bot.toot("J'ai rencontré une erreur, désooooo\n\nCette partie est annulée", id, True)
                del self.waitIDs[gameName]

            data[svcKey] = content

            self.bot.toot("Merci pour ta contribution, elle a été prise en compte", status, True)

            finished = True
            for key, value in data.items():
                if(value == ""):
                    finished = False


            if(finished):
                self.bot.toot("""J'ai demandé à @{u1}, @{u2} et @{u3} de faire un Cadavre Exquis et voilà ce que ça donne :

{ds} {dv} {dc}""".format( u1 = users[0]["acct"],
                                        u2 = users[1]["acct"],
                                        u3 = users[2]["acct"],
                                        ds = data["sujet"],
                                        dv =  data["verbe"],
                                        dc = data["complément"])   , None, False)
                del self.waitIDs[gameName]
                os.unlink(filename)
            else:
                with open(filename, 'wb') as output:  # Overwrites any existing file.
                    pickle.dump(data, output, pickle.HIGHEST_PROTOCOL)


        self.bot.sinceID = notification["id"]
        open(Consts.sinceID, 'w').write(str(self.bot.sinceID))




    def inList(self, id):
        for gameName, arrayData in self.waitIDs.items():
            if(id in arrayData[0]):
                index = arrayData[0].index(id)
                return True, index, gameName

        return False, 0, None
