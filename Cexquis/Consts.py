
gameDirectory = "data/games/"
 

appName = "DrawMeASheepApp"

appCred = "data/appCred.dat"
accountCred = "data/accountCred.dat"
apiBaseURLFile = "data/apiBaseURL.dat"
botNameFile = "data/botName.dat"
emergencyContactFile = "data/emergencyContact.dat"
proposeEmergencyContact = True
emergencyMessage = "Hey !\n J'ai rencontré une erreur récemment... Voilà, pour te mettre au courant... Bon courage !"
verboseBase = 10
verboseError = 10

debug = True

nameMaxLength = 32
tootLength = 500

log = "data/log/log.txt"

sinceID = "data/log/since_id.txt"

botName = "Cexquis"
