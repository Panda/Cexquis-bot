import mastodon
import os
import time

from TootHTMLParser import TootHTMLParser
import Consts
from BotListener import BotListener
import Log



#The bot that rocks. Can listen to events and publish toots !
class Bot(object):
    def __init__(self):
        #First we can connect using the api.
        api_base_url = open(Consts.apiBaseURLFile).read()
        self.apiMasto = mastodon.Mastodon(client_id = Consts.appCred, access_token = Consts.accountCred,api_base_url = api_base_url)
        Log.logprint("Bot connected.")

        #We'll treat every message since the last one we treated.
        if os.path.exists(Consts.sinceID) and open(Consts.sinceID).read().isdigit():
            self.sinceID = int(open(Consts.sinceID).read())
        else:
            self.sinceID = 0

        self.listener = BotListener(self)


    def toot(self, text, status, isDirect = True):
        CW = "CW : Mélange improbable et potentiellement dangereux"
        if(isDirect):
            CW = None

        atUser = ""
        if(status is not None and status["account"]["acct"] != Consts.botName):
            atUser =  '@' + status['account']['acct']
            text = atUser + " " + text

        if(isDirect):
            visibility = 'direct'
        else:
            visibility = 'public'


        reply = None

        if(status is not None):
            reply = status['id']
        status = self.apiMasto.status_post(text[:Consts.tootLength-1], in_reply_to_id=reply, visibility=visibility, spoiler_text = CW)
        return status

    #We just stream the notifications. Never ending function (in theory)
    def listen(self):
        Log.logprint("Listening for events since last time (ID " + str(self.sinceID) + ")")
        for notification in reversed(self.apiMasto.notifications(since_id=self.sinceID)):
            self.listener.on_notification(notification)


        Log.logprint("Now listening for new events...")
        self.apiMasto.stream_user(self.listener)

    def emergencyContact(self):
        contact = open(Consts.emergencyContactFile).read()
        self.apiMasto.status_post('@' + contact + " " + Consts.emergencyMessage, visibility = 'direct')
