import time
import traceback
import os
import sys
from getpass import getpass
from mastodon import Mastodon
from Bot import *
import Consts
import Log

 
if(not os.path.exists(Consts.appCred) or not os.path.exists(Consts.apiBaseURLFile)):
    Log.logprint("\n\nHello you ! It seems that it's the first time running this program. The creation of the bot account will begin now.\n")

    Log.logprint("The very first step is outside this program. You have to choose a Mastodon instance (example : https://botsin.space) and create the account for your bot. For that, give a name to the account, a log in email and a password.\n")

    Log.logprint("Once it's done, you need to give these informations to this program.\n")

    Log.logprint("First, you need to create the app to access Mastodon with this program. You only need to give the instance where you created the bot account :\n")

    api_base_url = input("Url of the mastodon instance (eg: https://botsin.space): ").strip()

    if not api_base_url.startswith(("https://", "http://")):
        Log.logprint("The url of the mastodon instance needs to start with either https:// or http://")
        sys.exit(1)

    #We create the app.
    Mastodon.create_app(
        Consts.appName,
        api_base_url=api_base_url,
        to_file=Consts.appCred
    )
    #We save the api_base_url in a file
    with open(Consts.apiBaseURLFile, "w+") as apiFile:
        apiFile.write(api_base_url)
    Log.logprint("\nGreat !")



if(not os.path.exists(Consts.accountCred)):
    Log.logprint("The app for mastodon is created. You now need to log in once through this program with the same email and password you used for the creation of the bot account.\n")
    login_email = input("Bot login email (eg: username@domain.com): ").strip()
    if "@" not in login_email:
        Log.logprint("The login email should be an email, not a username")
        sys.exit(1)

    account_password = "1"
    account_password2 = "2"
    while(account_password != account_password2):
        account_password = getpass("Bot password (it won't be stored): ").strip()
        account_password2 = getpass("Confirm Bot password : ").strip()
        if(account_password != account_password2):
            Log.logprint("The passwords are different. Try again.\n")


    Log.logprint("Trying to log in...")
    #Create api without log in to any account
    mastodon = Mastodon(
        client_id=Consts.appCred,
        api_base_url=api_base_url
    )

    loggedin = False
    while(not loggedin):
        try:
            mastodon.log_in(
                login_email,
                account_password,
                to_file=Consts.accountCred
            )

            with open(Consts.botNameFile, "w+") as botnamefile:
                botnamefile.write(mastodon.account_verify_credentials()["username"])
            loggedin = True
        except MastodonIllegalArgumentError:
            Log.logprint("\n\n[ERROR] Couldn't log in your account. Make sure : \n - the login email and the password are correct\n - or you can access your bot account manually\n - or you did have created the bot account before.")

    Log.logprint("Log in succeed.")

if(not os.path.exists(Consts.emergencyContactFile) and Consts.proposeEmergencyContact):
    tryFind = True
    while(tryFind == True):
        Log.logprint("\nOptionally, you can set up an account to contact if something goes wrong.\n")
        emergency_contact = input("username@domain.tld of the emergency contact (type \"none\" if you don't want an emergency contact): ").strip()
        if(emergency_contact is "none"):
            tryFind = False
        else:
            api_base_url = open(Consts.apiBaseURLFile).read()
            apiMasto = Mastodon(client_id = Consts.appCred, access_token = Consts.accountCred,api_base_url = api_base_url)
            res = apiMasto.account_search(emergency_contact)
            if(len(res) == 0):
                Log.logprint("\n[ERROR] Zero contact with this name found")
            elif(len(res) > 1):
                Log.logprint("\n[ERROR] Too many results corresponding to the given name.")
            else:
                tryFind = False
                with open(Consts.emergencyContactFile, "w+") as ecf:
                    ecf.write(res[0]["acct"])
                Log.logprint("\n" + res[0]["display_name"] + "(" + res[0]["acct"] + ") is set as an emergency contact.")

    Log.logprint("\n\nEverything is set ! The program will run normally now. Thank you for your cooperation !")



#creation of a bot that will always listen for new toots (and react to them...)
bot = Bot()

noErrors = True
while True:
    try:
        bot.listen()
    except Exception:
        Log.logprint(traceback.format_exc(), Consts.verboseError)

        if(noErrors and os.path.exists(Consts.emergencyContactFile)):
            try:
                bot.emergencyContact()
            except Exception:
                Log.logprint("[ERROR] Couldn't contact emergency contact without throwing an error", Consts.verboseError)
            noErrors = False

        Log.logprint("I will try to reconnect in 5 seconds...", Consts.verboseError)
        time.sleep(5)
